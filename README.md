# Sky Sentinel Proyect
*Proyecto para ACECOM*

## Vista General 📋
Sky Sentinel es un juego del género Shooter en tercera persona, inspirado en el juego de arcade Space Invaders, pero traído al mundo 3D, en el que buscas sobrevivir en un espacio hostil lleno de asteroides; evita chocarte con estos y destrúyelos. No es tan fácil, a dificultad incrementa rápidamente a medida que avanzas en el juego.

## Programas Utilizados 🔧

 - Unity : El motor de videojuegos usado (versión 2021.3.3f1). 
 - Visual Studio Code 2019 : Editor de código por defecto de Unity.

## Mecánicas⚙️
La mecánica principal del juego es destruir los asteroides evitando chocarse con estos, mientras que la velocidad de estos sube a medida que avanzas en puntaje.

 - Controles de movimiento: A (Izquierda), W (Arriba), S (Abajo) y D (Derecha).
 - Con Escape durante el juego regresas al menú principal.
 - Con click disparas en la dirección apuntada, solo cuando tienes un asteroide en la mira.
 - Ganas un punto por cada impacto ifligido al asteroide.
 - Por cada punto que obtienes:
    - La velocidad de los asteroides aumenta.
    - La aletoriedad en la variedad de velocidades aumenta.
    - La tasa de aparición de los asteroides aumenta.
 - La velocidad y tasa de aparición tienen un límite que no sobrepasan, pero el rango de velocidad aleatorio sí puede aumentar infinitamente.

### GAME DESIGN DOCUMENT 📌
Para más información se puede revisar el GDC. 
https://drive.google.com/file/d/1h23XIpNTMsMkeMxuNdLvidtkCnTxBU8t/view

Si desea probar el videojuego, puede descargar el ejecutable en .zip.
https://drive.google.com/file/d/1P-_D2HryudopHZURwtLPiJutz-2LoEKi/view?usp=sharing
