using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class ButtonManager : MonoBehaviour
{
    public Button NewGameBtn;
    public Button ToMainMenu;
    void Start()
    {
        NewGameBtn.onClick.AddListener(LoadSceneGame);
        ToMainMenu.onClick.AddListener(GoToMenu);
    }
    
    private void LoadSceneGame()
    {
        SceneManager.LoadScene(1);
    }
    private void GoToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
