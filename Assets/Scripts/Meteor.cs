using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Meteor : MonoBehaviour, IPointerClickHandler
{
    public float life = 3;
    public float damage = 3;
    public List<Gun> guns;
    public ParticleSystem BoomPS;
    public Rigidbody rbd;
    public UIManager UIManager;
    public AudioSource ExplotionSFX;
    public AudioClip boomClip;
    private Renderer MeshRenderer;
    
    private void Start() {
        MeshRenderer = GetComponent<Renderer>();
    }

    private void Update() {
        transform.Rotate(new Vector3(-50,25,0) * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider bullet) {
        if (bullet.CompareTag("Wall")){
            Destroy(gameObject);
        }
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {       
        foreach (var item in guns){
            item.Shoot(transform.position);
        }
        Damage();
        UIManager.AddScore(1);
    }

    private void Damage()
    {
        life--;
        MeshRenderer.material.color += new Color(0.5f, 0f, 0f, 0f);
        //Se enrojecerá cada vez que toma daño.
        if(life <= 0)
        {
            UIManager.GeneralAudioSource(boomClip);
            StartCoroutine(DestroyCoroutine());
        }
    }

    private IEnumerator DestroyCoroutine()
    {
        BoomPS.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        Destroy(gameObject);
    }
    
}
