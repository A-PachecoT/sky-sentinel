using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public float laserOffTime = 0.25f;  
    // [SerializeField] float maxDistance = 150f;  //Máxima longitud de la línea; Serialize hace a la variable pública, pero no editable
    public Transform bulletSpawnPoint;
    public LineRenderer laser;
    // bool canFire = true; //Esperar para poder disparar nuevamente
    public GameObject GunModel;  //Incorporar movimiento giratorio en las torretas
    public AudioSource shootingSource;
    public AudioClip shootingS;
    public Light shootingLight;

    void Start()
    {
        TurnOffLaser();
    }

    public void Shoot(Vector3 target)
    {
        StopAllCoroutines();
        TurnOffLaser();
        StartCoroutine(DrawLaser(target));
        Vector3 direction = target - GunModel.transform.position;
        GunModel.transform.up = direction.normalized;
        shootingSource.PlayOneShot(shootingS);
        shootingLight.enabled = true;
    }
    
    private IEnumerator DrawLaser(Vector3 target)
    {
        laser.SetPosition(0, bulletSpawnPoint.position);
        laser.SetPosition(1, target);
        laser.enabled = true;
        yield return new WaitForSeconds(laserOffTime); 
        TurnOffLaser();
    }

    void TurnOffLaser()
    {
        laser.enabled = false;
        shootingLight.enabled = false;
    }

}
