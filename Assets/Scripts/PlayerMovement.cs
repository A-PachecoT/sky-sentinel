using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;  //Para volver al menú

public class PlayerMovement : MonoBehaviour
{
    // public CharacterController controller;
    Rigidbody m_Rigidbody;
    public UIManager UIManager;
    public Animator anim;
    public float XY_acceleration = 1f;
    public float Z_acceleration = 9f;
    public float life = 30f;

    public List<AudioSource> audioSource;
    public AudioClip Hit;
    public AudioClip DeadSound;
    public AudioClip MeteorBoom;
    public ParticleSystem MeteorExplotion;

    private void Start() {
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");  //Entre -1 y 1 en el eje x
        float vertical = Input.GetAxisRaw("Vertical");      //Entre -1 y 1 en el eje y
        float enfrente = 0f;
        // if (Input.GetKey(KeyCode.Space))    //Nos permite propulsarse hacia adelante
        //     enfrente = 1f;  
        if (Input.GetKey(KeyCode.Escape))   
            SceneManager.LoadScene(0);  //Salir al menú principal


            anim.SetBool("PressR", (horizontal > 0));
            anim.SetBool("PressL", (horizontal < 0));

            anim.SetBool("PressU", (vertical > 0));
            anim.SetBool("PressD", (vertical < 0)); 

            // if (horizontal != 0 && vertical != 0)    //Active Thrusters
            // else 
    
    
               

        Vector3 direction = new Vector3(horizontal, vertical, 0f).normalized * XY_acceleration + new Vector3(0f,0f, enfrente * Z_acceleration);
        //Normalizamos para que el jugador no se mueva más rápido al presionar dos teclas(en diagonal).

        if (direction.magnitude > 0f)
        {
            m_Rigidbody.AddForce(direction);
        };
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Meteor")){
            life -= other.GetComponent<Meteor>().damage;
            UIManager.LifeDisplay(life);
            audioSource[0].PlayOneShot(Hit);
            MeteorExplode(other);
            if(life <= 0)   PlayerExplode();
        }
    }

    public void PlayerExplode()
    {
        audioSource[1].PlayOneShot(DeadSound);
        Destroy(gameObject);
    }
    public void MeteorExplode(Collider meteor)
    {
        audioSource[2].PlayOneShot(MeteorBoom);
        Destroy(meteor.gameObject);
        ParticleSystem boomParticles = Instantiate(MeteorExplotion,gameObject.transform.position,Quaternion.identity); 
    }

}
