using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private int score = 0;
    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private Text finalScoreText;
    [SerializeField]
    private Text lifeText;

    public PlayerMovement player;
    public Spawner Spawner;
    public int increment = 5;
    GameObject MainCanvas;
    GameObject GameOverCanvas;
    public AudioSource GAudioSource;
    public ParticleSystem PSystem;

    private void Awake() {
        MainCanvas = GameObject.Find ("MainCanvas");
        GameOverCanvas = GameObject.Find ("GameOverCanvas");
        MainCanvas.SetActive(true);
        GameOverCanvas.SetActive(false);
    }
    void Start()    
    {
        scoreText.text = "Score: " + 0;   
        lifeText.text = "Life: " + player.life;
    }

    public void AddScore (int points)
    {
        score += points;
        scoreText.text = "Score: " + score.ToString();
        Spawner.AddMeteorVelocity(increment);
        // PSystem.main.simulationSpeed += 10;
    }
    public void LifeDisplay (float life)
    {
        lifeText.text = "Life: " + life.ToString();
        if (player.life <= 0)   CanvasSwap();
    }

    private void CanvasSwap()
    {
        finalScoreText.text = "Score: " + score.ToString();
        MainCanvas.SetActive(false);
        GameOverCanvas.SetActive(true);
    }

    public void GeneralAudioSource(AudioClip clip)
    {
        GAudioSource.PlayOneShot(clip);
    }

}
