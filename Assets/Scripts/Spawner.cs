using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public List<Transform> SpawnPoints;
    public List<Meteor> MeteorPrefabs;
    public List<Gun> guns;
    public UIManager UIManager;
    public float velocity = 10;
    public float spawnrate = 10;
    public float MAXvelocity = 150f;
    public float MAXspawnrate = 0.1f;
    public int randomSpawnRange = 15;
    private int randomVelocityRange = 2;

    
    void Start()
    {
        StartCoroutine(SpawnCoroutine());
    }

    void SpawnMeteor()
    {
        int indexPoint = Random.Range(0,SpawnPoints.Count);
        int indexMeteor = Random.Range(0,MeteorPrefabs.Count);
        //Agregando variedad a la velocidad del asteroide instanciado:
        float newVelocity = velocity + Random.Range(0, randomVelocityRange);
        //Agregando aleatoriedad a los spawnpoints de la lista:
        Vector3 spawnpoint = SpawnPoints[indexPoint].position + new Vector3(Random.Range(-1*randomSpawnRange,randomSpawnRange),Random.Range(-1*randomSpawnRange,randomSpawnRange), 0);

        Meteor tempMeteor = Instantiate(MeteorPrefabs[indexMeteor],spawnpoint,Quaternion.identity,SpawnPoints[indexPoint]);
        tempMeteor.guns = guns;
        tempMeteor.UIManager = UIManager;
        tempMeteor.rbd.velocity = SpawnPoints[indexPoint].forward * newVelocity;
    }

    private IEnumerator SpawnCoroutine()
    {
        while(true)
        {
            SpawnMeteor();
            yield return new WaitForSeconds(spawnrate);     
        }
    }

    public void AddMeteorVelocity(int increment)
    {
        velocity += increment;
        randomVelocityRange += 1;
        spawnrate -= 0.005f;
        if (velocity >= MAXvelocity)
        {
            Debug.Log("Max speed reached");
            velocity = MAXvelocity;
        }
        if (spawnrate <= MAXspawnrate)
        {
            Debug.Log("Max spawn rate reached");
            spawnrate = MAXspawnrate;
        }
    }
}
